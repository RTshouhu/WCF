﻿using System;
using System.ServiceModel;
using System.Collections.Generic;

namespace DualBindingServer
{
    [ServiceBehavior(ConcurrencyMode=ConcurrencyMode.Reentrant)]
    public class HelloWCFService : IHelloWCFService
    {
        static List<IHelloWCFServiceCallback> HelloWCFList = new List<IHelloWCFServiceCallback>();
        static List<IHelloWCFServiceCallback> HelloWCFOneWayList = new List<IHelloWCFServiceCallback>();
        public void HelloWCF(string inputMsg)
        {
            IHelloWCFServiceCallback callback = OperationContext.Current.GetCallbackChannel<IHelloWCFServiceCallback>();
            HelloWCFList.Add(callback);
            if (HelloWCFList.Count > 2)
            {
                for (int i = 0; i < HelloWCFList.Count; i++)
                {
                    callback.HelloWCFCallback(inputMsg);
                }

                HelloWCFList.Clear();
            }            
        }

        public void HelloWCFOneWay(string inputMsg)
        {
            IHelloWCFServiceCallback callback = OperationContext.Current.GetCallbackChannel<IHelloWCFServiceCallback>();
            HelloWCFOneWayList.Add(callback);
            callback.HelloWCFCallbackOneWay(inputMsg);
        }
    }
}
