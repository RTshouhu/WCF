﻿using System;
using System.ServiceModel;

namespace DualBindingServer
{
    [ServiceContract(Namespace = "http://schemas.xinhaijulan.com/demos/DualHttpBinding", CallbackContract = typeof(IHelloWCFServiceCallback))]
    public interface IHelloWCFService
    {
        [OperationContract]
        void HelloWCF(string inputMsg);

        [OperationContract(IsOneWay = true)]
        void HelloWCFOneWay(string inputMsg);
    }

    public interface IHelloWCFServiceCallback
    {
        [OperationContract]
        void HelloWCFCallback(string msg);

        [OperationContract(IsOneWay = true)]
        void HelloWCFCallbackOneWay(string msg);
    }
}
