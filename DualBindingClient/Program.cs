﻿using System;
using System.Threading;
using System.ServiceModel;

namespace DualBindingClient
{
    class Program : DualbindingService.IHelloWCFServiceCallback
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Client running on thread {0}" + Environment.NewLine, Thread.CurrentThread.GetHashCode());

            Program program = new Program();
            InstanceContext instanceContext = new InstanceContext(program);

            using (DualbindingService.HelloWCFServiceClient client = new DualbindingService.HelloWCFServiceClient(instanceContext))
            {
                // 方法1
                //WSDualHttpBinding binding = client.Endpoint.Binding as WSDualHttpBinding;
                //binding.ClientBaseAddress = new Uri("http://localhost:8080");
                
                // 方法二
                System.ServiceModel.WSDualHttpBinding binding1 = new WSDualHttpBinding();
                //binding1.ProxyAddress = new Uri("http://localhost:8732/Design_Time_Addresses/DualBindingServer/HelloWCFService/HelloWCF");
                //binding1.ClientBaseAddress = new Uri("http://localhost:8080");
                var service = new DuplexChannelFactory<DualBindingClient.DualbindingService.IHelloWCFService>(new InstanceContext(new Program()), binding1, new EndpointAddress(new Uri("http://localhost:8732/Design_Time_Addresses/DualBindingServer/HelloWCFService/HelloWCF")));
                var service1 = service.CreateChannel();
                Console.WriteLine("Call HelloWCF Begin");
                service1.HelloWCF("Hello WCF");
                Console.WriteLine("Call HelloWCF End");

                Console.WriteLine("Call HelloWCF Begin");
                service1.HelloWCF("Hello WCF");
                Console.WriteLine("Call HelloWCF End");

                //Console.WriteLine("Call HelloWCF Begin");
                //service1.HelloWCF("Hello WCF");
                //Console.WriteLine("Call HelloWCF End");

                //Console.WriteLine();
                //Console.WriteLine("Call HelloWCF OneWay Begin");
                //service1.HelloWCFOneWay("Hello WCF OneWay");
                //Console.WriteLine("Call HelloWCF OneWay End");

                //因使用了OneWay标记属性的方法：只要调用的方法开始执行，就会继续执行下面的方法，不会阻塞
                //所以，此时如果不使用等待输入ReadLine()操作，using方法体应付执行结束，那么client对象也就会close，并释放
                //然而此时Callback方法还并未被执行，因此，若不写下面的ReadLine()，则在执行Callback方法时，
                //会报异常：Additional information: The session was closed before message transfer was complete.。
                Console.ReadLine();
            }

            Console.ReadLine();
        }

        public void HelloWCFCallback(string msg)
        {
            Console.WriteLine("HelloWCFCallback on thread {0}, and the message is:{1}", Thread.CurrentThread.GetHashCode(), msg);
        }

        public void HelloWCFCallbackOneWay(string msg)
        {
            Console.WriteLine("HelloWCFCallbackOneWay on thread {0}, and the message is:{1}", Thread.CurrentThread.GetHashCode(), msg);
        }
    }
}